import { configureStore } from '@reduxjs/toolkit'
import {userSlice} from "./stateManagement/userSlice";

const store = configureStore({
  reducer: userSlice.reducer,
})

export default store
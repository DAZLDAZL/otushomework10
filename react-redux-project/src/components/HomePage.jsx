import React from 'react';
import ReactDOM from 'react-dom/client';
import 'bootstrap/dist/css/bootstrap.css';
import { Link } from 'react-router-dom';

function HomePage(){
    return(
        <div className='container text-center' >
            <div className='row'>
                <p className='text-center h1'>Главная Страница</p>
            </div>
        
            <div className='row' style={{width:30 + '%', marginLeft:35 + '%'}}>
                <div className='col'>
                    <Link to='/Login' className='btn btn-primary'>Войти</Link>
                </div>
                <div className='col'>
                    <Link to='/Register' className='btn btn-primary'>Регистрация</Link>
                </div>
            </div>
        </div>
    );
}

export default HomePage;
import React, { useState } from 'react';
import ReactDOM from 'react-dom/client';
import { useDispatch, useSelector } from 'react-redux';
import { userRegister } from '../stateManagement/userSlice';
let user = {
    Login: "",
    Password: ""
};

function Register(){
    const dispatch = useDispatch();
    

    const [user, setData] = useState({ Login: "", Password: "" });
    const handleChange = (e) => setData({ ...user, [e.target.name]: e.target.value });

    const registredUser = useSelector((state) => state.registredUser);

    const reg = () => {
        
       dispatch(userRegister(user));
       console.log(`зарегестрирован ${registredUser.Login} : ${registredUser.Password}`);
    }

    return(<div>
        <p>Страница регистрации</p>
        <form>
            <input type='text' name='Login' onChange={e => handleChange(e)}/>
            <input type='password' name='Password' onChange={e => handleChange(e)}/>
            <button onClick={reg()}>Зарегистрироваться</button>
        </form>
        </div>
    );
}

export default Register;
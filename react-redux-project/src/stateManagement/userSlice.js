import { createSlice, current, PayloadAction } from "@reduxjs/toolkit";

export const userSlice = createSlice({
  name: "user",
  initialState: {
    currentUser: {},
    registredUser: {}
  },
  reducers: {
    userLogin: (state, action) => {
      console.log(action);
      if(action.payload.Login == state.registredUser.Login && action.payload.Password == state.registredUser.Password){
        state.currentUser = structuredClone(state.registredUser);
      }
    },
    userLogout: (state) => {
      state.currentUser = null;
    },
    userRegister: (state,action) => {
      state.registredUser = action.payload;
      state.currentUser = state.registredUser;
    },
  },
});

// each case under reducers becomes an action
export const { userLogin, userLogout, userRegister } = userSlice.actions;

export default userSlice.reducer;


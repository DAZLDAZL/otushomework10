import logo from './logo.svg';
import './App.css';
import { Route, BrowserRouter as Router, Routes } from "react-router-dom";
import HomePage from "./components/HomePage";
import Login from "./components/Login";
import Register from "./components/Register";
import NotFound404 from './components/NotFound404';

function App() {
  return (
    <Router>
      <Routes>
        <Route index Component={HomePage}/>
        <Route path="login" Component={Login}/>
        <Route path='register' Component={Register}/>
        <Route path='*' Component={NotFound404}/>
      </Routes>
    </Router>
  );
}

export default App;
